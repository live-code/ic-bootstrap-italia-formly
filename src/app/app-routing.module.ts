import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'bootstrap-italia-demo',
    loadChildren: () => import('./features/bootstrap-italia-demo/bootstrap-italia-demo.module').then(m => m.BootstrapItaliaDemoModule)
  },
  { path: 'formly-demo1', loadChildren: () => import('./features/formly-demo1/formly-demo1.module').then(m => m.FormlyDemo1Module) },
  { path: 'formly-demo2', loadChildren: () => import('./features/formly-demo2/formly-demo2.module').then(m => m.FormlyDemo2Module) },
  { path: 'formly-demo3', loadChildren: () => import('./features/formly-demo3/formly-demo3.module').then(m => m.FormlyDemo3Module) },
  { path: '', redirectTo: 'formly-demo3', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
