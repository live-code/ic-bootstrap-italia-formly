import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyModule } from '@ngx-formly/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    SharedModule,
    FormlyModule.forRoot({
    /*  types: [
        {
          name: 'input',
          wrappers: ['input-wrapper'],
          component: MyInputComponent
        },
      ],
      wrappers: [
        {
          name: 'input-wrapper',
          component: MyInputWrapperComponent
        },
      ],*/
    }),
    FormlyBootstrapModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
