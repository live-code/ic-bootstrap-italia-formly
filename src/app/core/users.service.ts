import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getUsers(text: string) {
    // console.log('get user this', this)
    return this.http.get<any[]>('https://jsonplaceholder.typicode.com/users?q=' + text)
      .pipe(
        tap(res => console.log('get user result', res))
      )
  }


}
