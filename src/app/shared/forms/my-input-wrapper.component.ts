import { Component } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
  selector: 'app-my-input-wrapper',
  template: `
    <!--<h1>CUSTOM WRAPPER</h1>-->
    <div class="card bg-secondary m-3">
      <label for="{{ props.label }}" class="card-header">{{props.label}}</label>
      <div class="card-body">
        <ng-container #fieldComponent></ng-container>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class MyInputWrapperComponent extends FieldWrapper{

}
