import { HttpClient } from '@angular/common/http';
import { Component, inject, Injector } from '@angular/core';
import { FieldType, FieldTypeConfig } from '@ngx-formly/core';
import { debounceTime, mergeMap } from 'rxjs';
import { UsersService } from '../../core/users.service';
import { FormUtilsService } from '../../features/formly-demo2/services/form-utils.service';

@Component({
  selector: 'app-my-input',
  template: `
    <input type="input" [formControl]="formControl" [formlyAttributes]="field">
  `,
  styles: [
  ]
})
export class MyInputComponent extends FieldType<FieldTypeConfig>{

  constructor(private formUtils: FormUtilsService) {
    super();
  }

  ngOnInit() {
    // TODO: vedere se possibile gli hooks
    // this.field.hooks.

    this.formUtils.changeHandler(this.field.props, this.formControl)
      .subscribe((res: any) => {
        this.field.parent?.form?.get('lastName')?.disable()
        // TODO: gestire piu casistiche -> ad es. set validators
      })

  }
}






/*
function searchUser(text: string) {
  const http = inject(HttpClient);
  return http
    .get<any[]>('https://jsonplaceholder.typicode.com/users?q=' + text)
}*/
