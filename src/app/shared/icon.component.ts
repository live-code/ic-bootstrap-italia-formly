import { Component, Input } from '@angular/core';

@Component({
  // selector: 'app-icon[icon]',
  selector: 'app-icon',
  template: `
    <svg [ngClass]="['icon', 'icon-' + size]">
      <use [attr.href]="'/bootstrap-italia/svg/sprites.svg#' + icon"></use>
    </svg>

  `,
  styles: [
  ]
})
export class IconComponent {
  @Input() icon!: string;
  @Input() size: 'xs' | 'lg' | 'xl' = 'xs'
}
