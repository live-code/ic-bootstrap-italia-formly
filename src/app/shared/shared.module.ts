import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { IconComponent } from './icon.component';
import { MyInputComponent } from './forms/my-input.component';
import { MyInputWrapperComponent } from './forms/my-input-wrapper.component';



@NgModule({
  declarations: [IconComponent, MyInputComponent, MyInputWrapperComponent],
  exports: [IconComponent, MyInputComponent, MyInputWrapperComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormlyModule.forChild(),
  ]
})
export class SharedModule { }
