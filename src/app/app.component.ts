import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <button routerLink="formly-demo3" routerLinkActive="bg-dark text-white">Formly Demo</button>
    <button routerLink="bootstrap-italia-demo" routerLinkActive="bg-dark text-white">Bootstrap Italia Demo</button>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {

}


