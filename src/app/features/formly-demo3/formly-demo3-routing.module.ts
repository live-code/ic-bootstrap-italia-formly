import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormlyDemo3Component } from './formly-demo3.component';

const routes: Routes = [{ path: '', component: FormlyDemo3Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormlyDemo3RoutingModule { }
