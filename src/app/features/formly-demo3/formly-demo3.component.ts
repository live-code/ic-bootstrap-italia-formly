import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { combineLatest } from 'rxjs';
import { FormGeneratorService } from './form-uikit/services/form-generator.service';

@Component({
  selector: 'app-formly-demo3',
  template: `
    <form [formGroup]="form" (ngSubmit)="submit()">
      <formly-form [model]="model" [fields]="formUtilsService.formConfig" [options]="options" [form]="form"></formly-form>
      
      <button 
        submit-button type="submit"
        [disabled]="form.invalid || form.pending"
        class="btn btn-primary submit-button">Submit</button>

      {{form.valid}}
    </form>

    <pre>{{form.value | json}}</pre>
  `,
  providers: [
    FormGeneratorService
  ]
})
export class FormlyDemo3Component  {
  form = new FormGroup({});
  options: FormlyFormOptions = {};
  fields!: FormlyFieldConfig[];
  model: any; // Custom Form Model Type

  constructor(
    private http: HttpClient,
    public formUtilsService: FormGeneratorService,
  ) {
    combineLatest({
      // form configuration
      form: http.get<any>('http://localhost:3000/client_form'),
      // form data
      data: http.get<any>('http://localhost:3000/user'),
    })
      .subscribe(res => {
        // generate form
        this.formUtilsService.generator(res.form);
        // populate form
        this.model = res.data;
      });
  }

  submit() {
    if (this.form.valid) {
      alert(JSON.stringify(this.model));
    }
  }


}
