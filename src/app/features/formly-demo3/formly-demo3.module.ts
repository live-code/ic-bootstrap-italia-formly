import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { CustomCheckbox } from './form-uikit/components/custom-checkbox';
import { CustomSelect } from './form-uikit/components/custom-select';
import {
  checkUserName,
  IpValidator,
  IpValidatorMessage,
  minLengthValidatorMessage
} from './form-uikit/validators/sync.validators';

import { FormlyDemo3RoutingModule } from './formly-demo3-routing.module';
import { FormlyDemo3Component } from './formly-demo3.component';
import { CustomFieldWrapper } from './form-uikit/components/custom-field-wrapper.component';
import { CustomInputText } from './form-uikit/components/custom-input-text';


@NgModule({
  declarations: [
    FormlyDemo3Component,
    CustomInputText,
    CustomFieldWrapper,
    CustomCheckbox,
    CustomSelect
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormlyDemo3RoutingModule,
    FormlyModule.forChild({
      validators: [
        { name: 'ip', validation: IpValidator },
        { name: 'checkUsername', validation: checkUserName},
      ],
      validationMessages: [
        { name: 'checkUsername', message: 'username wrong' },
        { name: 'ip', message: IpValidatorMessage },
        { name: 'required', message: 'This field is required' },
        { name: 'minLength', message: minLengthValidatorMessage },
      ],
      types: [
        {
          name: 'input',
          wrappers: ['app-custom-field-wrapper'],
          component: CustomInputText
        },
        {
          name: 'select',
          wrappers: ['app-custom-field-wrapper'],
          component: CustomSelect
        },
        {
          name: 'checkbox',
          wrappers: ['app-custom-field-wrapper'],
          component: CustomCheckbox
        },
      ],
      wrappers: [
        {
          name: 'app-custom-field-wrapper',
          component: CustomFieldWrapper
        },
      ],
    }),
  ]
})
export class FormlyDemo3Module { }

