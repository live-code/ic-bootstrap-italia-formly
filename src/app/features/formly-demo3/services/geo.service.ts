import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { FieldTypeConfig } from '@ngx-formly/core';
import { tap } from 'rxjs';
import { getFormlyField, updateSelectOptions } from '../form-uikit/services/formly.utils';

@Injectable({
  providedIn: 'root'
})
export class GeoService {

  constructor(private http: HttpClient) { }

  getRegions(input: AbstractControl, formlyField: FieldTypeConfig, customParam?: any) {
    // NOTE: any può essere facilmente sostituito con un type custom che rappresenti una regione
    return this.http.get<any[]>('http://localhost:3000/getRegions')
      .pipe(
        tap(data => {
          formlyField.props = { ...formlyField.props, items: data };
        })
      )
  }

  getCities(input: AbstractControl, formlyField: FieldTypeConfig, customParam?: any) {
    const currentValue = formlyField.parent?.form?.get(customParam)?.value

    // NOTE: any può essere facilmente sostituito con un type custom che rappresenti City
    return this.http.get<any[]>(`http://localhost:3000/getCities?regionId=${currentValue}`)
      .pipe(
        tap((data) => {
          updateSelectOptions(formlyField, 'cities', data)
          getFormlyField('cities', formlyField)?.setValue('');
          getFormlyField('cities', formlyField)?.enable();
        })
      )
  }

}
