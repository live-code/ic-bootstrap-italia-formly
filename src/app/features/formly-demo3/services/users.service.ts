import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { FieldTypeConfig } from '@ngx-formly/core';
import { tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  /**
   * The returned observable is automatically subscribed
   * @param input
   * @param formlyField
   */
  getUsers(input: AbstractControl, formlyField: FieldTypeConfig, customParam?: any) {
    return this.http.get<any[]>('https://jsonplaceholder.typicode.com/users?q=' + input.value)
      .pipe(
        tap(res => {
          console.log('get user result in UserService', res)
        })
      )
  }
}
