import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { FieldTypeConfig } from '@ngx-formly/core';

@Injectable({
  providedIn: 'root'
})
export class MiscService {

  doSomething(input: AbstractControl, formlyField: FieldTypeConfig, customParam?: any) {
    console.log(' do something', customParam)
  }

  doSomethingElse(input: AbstractControl, formlyField: FieldTypeConfig, customParam?: any) {
    console.log('onInitQualcosa', input, formlyField)
    // window.alert('init ' + customParam);
  }

  disableField(input: AbstractControl, formlyField: FieldTypeConfig, customParam?: any) {
    // console.log(input.value)
    if (input.value) {
      //formlyField.parent?.form?.get('lastName')?.disable()
      formlyField.parent?.form?.get(customParam)?.disable()
    } else {
      formlyField.parent?.form?.get(customParam)?.enable()
    }
  }


}
