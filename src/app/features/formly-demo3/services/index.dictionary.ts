import { GeoService } from './geo.service';
import { MiscService } from './misc.service';
import { UsersService } from './users.service';

export const SERVICES: { [key: string]: any } = {
  ['MiscService']: MiscService,
  ['UsersService']: UsersService,
  ['GeoService']: GeoService,
}

