import { AbstractControl, ValidationErrors } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';


export function minLengthValidatorMessage(error: any, field: FormlyFieldConfig) {
  return `Il campo deve avere ALMENO ${field.props?.minLength} caratteri`;
}

export function IpValidatorMessage(error: any, field: FormlyFieldConfig) {
  return `"${field.formControl!.value}" is not a valid IP Address`;
}

export function IpValidator(control: AbstractControl): ValidationErrors | null {
  return /(\d{1,3}\.){3}\d{1,3}/.test(control.value) ? null : { 'ip': true };
}

export function checkUserName() {
  return fetch('https://jsonplaceholder.typicode.com/users');
}
