import { HttpClient } from '@angular/common/http';
import { AbstractControl } from '@angular/forms';
import { map, of } from 'rxjs';


export const checkUsername = (control: AbstractControl, http: HttpClient) => {
  // NOTE: any può essere facilmente sostituito con un type custom che rappresenti uno User
  return http.get<any[]>('https://jsonplaceholder.typicode.com/users?username='+ control.value)
    .pipe(
      map(res => !res.length)
    )
}

export const checkHello = (control: AbstractControl, http: HttpClient) => {
  return of(!(control.value === 'hello'))
}


export const ASYNC_VALIDATORS: { [key: string]: any } = {
  checkUsername: checkUsername,
  checkHello: checkHello,
}
