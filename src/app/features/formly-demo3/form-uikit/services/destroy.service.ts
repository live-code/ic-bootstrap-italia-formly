import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class DestroyService {
  destroy$ = new Subject();

  ngOnDestroy() {
    console.log('destroy')
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
