/**
 * This method can be used to update the options of a list
 * @param formlyField
 * @param key
 * @param data
 */
export function updateSelectOptions(formlyField: any, key: string, data: any) {
  const index = formlyField.parent.fieldGroup.findIndex((field: any) => field.key === key)
  const cityField = formlyField.parent.fieldGroup[index];
  cityField.props! = {
    ...cityField.props!,
    items: data
  };
}

/**
 * Short way to get a formly field from parent
 * @param key
 * @param formlyField
 */
export function getFormlyField(key: string, formlyField: any) {
  return formlyField.parent?.form?.get(key);
}
