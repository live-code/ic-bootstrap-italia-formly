import { HttpClient } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { FieldTypeConfig } from '@ngx-formly/core';
import { FormlyFieldConfig } from '@ngx-formly/core/lib/models';
import { debounceTime, distinctUntilChanged, EMPTY, identity, mergeMap, Observable, } from 'rxjs';
import { ASYNC_VALIDATORS } from '../validators/async.validators';
import { SERVICES } from '../../services/index.dictionary';

export type CustomAsyncValidators = {
  validator: string;
  message: string;
}

export type CustomFormlyType = {
  myAsyncValidators: CustomAsyncValidators[]
} & FieldTypeConfig


@Injectable({ providedIn: 'root'})
export class FormGeneratorService {
  /**
   * State for formly configuration retrieved by JSON
   */
  formConfig: FormlyFieldConfig[] = [];

  constructor(private injector: Injector, private http: HttpClient) {}

  /**
   * Generate form from JSON
   * This populates and passes the formConfig props to formly
   *
   * @param JSON    any[]   //  it should be typed as the single field returned by the JSON
   */
  generator(JSON: CustomFormlyType[]) {
    const self = this;
    // JSON object iteration
    JSON.forEach(configItem => {
      const customItemConfig: FormlyFieldConfig = {
        // clone configuration item
        ...configItem,
        // set async validators from JSON
        asyncValidators: configItem['myAsyncValidators']?.reduce((acc: any, configItem) => {
         acc[configItem.validator] =  {
           expression: (c: any) => ASYNC_VALIDATORS[configItem.validator](c, self.http),
           message: configItem.message
         }
         return acc;
       }, {})
      }
      this.formConfig.push(customItemConfig)
    })
  }

  // ***********************
  // EVENT HANDLER
  // ***********************

  /**
   * Init field event listener
   * Must be invoked by each custom form field control
   * @param field       the field JSON obj
   * @param input      the form control of the field
   */
  initField(formlyField: FieldTypeConfig, input: AbstractControl) {
    this.ontInitHandler(formlyField, input)
    this.onBlurHandler(formlyField, input)
    this.onChangeHandler(formlyField, input)
  }

  ontInitHandler(formlyField: FieldTypeConfig, input: AbstractControl) {
    return this.invokeServiceMethodFromJSON('onInit', formlyField, input);
  }

  onBlurHandler(formlyField: FieldTypeConfig, input: AbstractControl) {
    return this.invokeServiceMethodFromJSON('onBlur', formlyField, input);
  }

  onChangeHandler(formlyField: FieldTypeConfig, input: AbstractControl) {
    return this.invokeServiceMethodWithPipeFromJSON('onChange', formlyField, input)
  }

  // ***********************
  // UTILS
  // ***********************

  invokeServiceMethodFromJSON(propEvenType: string, formlyField: FieldTypeConfig, input: AbstractControl) {
    const eventObj = formlyField?.props[propEvenType];

    if (eventObj) {
      try {
        // i.e. service: "UsersService"
        const serviceKeyFromJSON = eventObj['service']
        // inject the Service getting its reference from the the dictionary
        const serviceClassToUse = this.injector.get(SERVICES[serviceKeyFromJSON]);
        // i.e. fn: getUsers
        const fnFromJSON = eventObj['fn'];

        const invokeFn: Observable<any> =
          serviceClassToUse[fnFromJSON].apply(serviceClassToUse, [input, formlyField, eventObj['params']])

        try {
          invokeFn.subscribe();
        } catch(e) {}
      }
      catch(e) {
        throw new Error(`ERROR!!! The JSON of the "${formlyField.props.label || input.value }" field has problems`);
      }
    }
  }



  invokeServiceMethodWithPipeFromJSON(propEvenType: string, formlyField: FieldTypeConfig, input: AbstractControl) {
    const eventObj = formlyField?.props[propEvenType];
    // 'onChange' prop is defined
    if (eventObj) {
      try {
        // i.e. service: "UsersService"
        const serviceKeyFromJSON = eventObj['service']
        // inject the Service getting its reference from the the dictionary
        const serviceClassToUse = this.injector.get(SERVICES[serviceKeyFromJSON]);
        // i.e. fn: getUsers
        const fnFromJSON = eventObj['fn'];

        input.valueChanges
          .pipe (
            // debounce 'prop'
            debounceTime(eventObj['debounce'] || 0),
            // distinct 'prop'
            eventObj['distinct'] ? distinctUntilChanged() : identity,
            // invoke service + fn + custom param (defined by the JSON)
            // NOTE: the invoked service method can return an observable (automatically subscribed here) or nothing
            mergeMap(() => {
              return serviceClassToUse[fnFromJSON].apply(serviceClassToUse, [input, formlyField, eventObj['params']]) || EMPTY
            }),
          )
          .subscribe()
      }
      catch(e) {
        throw new Error(`ERROR!!! The JSON of the "${formlyField.props.label || input.value }" field has problems`);
      }
    }

    return EMPTY;
  }

}


