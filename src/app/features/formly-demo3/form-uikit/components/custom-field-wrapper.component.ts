import { Component } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
  selector: 'app-custom-field-wrapper',
  template: `
    <div>
      <h4>{{props.label}} <span *ngIf="props.required">*</span> </h4>
      <div *ngIf="props['requiredMessage']">
        {{props['requiredMessage']}}
      </div>

      
      <formly-validation-message
        *ngIf="formControl.errors"
        [field]="field"
        class="bg-danger rounded-3 text-white px-2"
      ></formly-validation-message>

      <div class="card-body">
        <ng-container #fieldComponent></ng-container>
      </div>
      
      <div>{{props.description}}</div>
      
      <hr>
    </div>
  `,
  styles: [`
  
  `]
})
export class CustomFieldWrapper extends FieldWrapper{

}
