import { Component, OnInit } from '@angular/core';
import { FieldType, FieldTypeConfig } from '@ngx-formly/core';
import { FormGeneratorService } from '../services/form-generator.service';

@Component({
  selector: 'app-custom-checkbox',
  template: `
    <input type="checkbox" [formControl]="formControl" [formlyAttributes]="field">
  `,
})
export class CustomCheckbox extends FieldType<FieldTypeConfig> implements OnInit {

  constructor(private formUtils: FormGeneratorService) {
    super();
  }

  ngOnInit() {
    this.formUtils.initField(this.field, this.formControl);
  }
}


