import { Component, OnInit } from '@angular/core';
import { FieldType, FieldTypeConfig } from '@ngx-formly/core';
import { DestroyService } from '../services/destroy.service';
import { FormGeneratorService } from '../services/form-generator.service';

@Component({
  selector: 'app-custom-input-text',
  template: `
    <input 
      (blur)="formUtils.onBlurHandler(field, formControl)" 
      type="input" [formControl]="formControl" [formlyAttributes]="field">
  `,
  providers: [
    DestroyService
  ]
})
export class CustomInputText extends FieldType<FieldTypeConfig> implements OnInit {

  constructor(public formUtils: FormGeneratorService, private destroySrv: DestroyService) {
    super();
  }

  ngOnInit() {
    this.formUtils.initField(this.field, this.formControl);

    if (this.field.props.blur) {
      this.field.props.blur(this.field)
    }

    /**
     * Set field to 0 when the text is empty
     */
    this.formControl.valueChanges
      .subscribe(res => {
        if (res === '' && this.field.props['defaultToZero']) {
          this.formControl.setValue(0)
        }
      })
  }

}
