import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FieldType, FieldTypeConfig } from '@ngx-formly/core';
import { FormGeneratorService } from '../services/form-generator.service';

@Component({
  selector: 'app-custom-select',
  template: `
  <select
    [formControl]="formControl" 
    [formlyAttributes]="field"
  >
    <option value="" disabled="true">{{props['defaultLabel']}}</option>
    <option [value]="option.id" *ngFor="let option of field.props['items']">
      {{option.value}}
    </option>
  </select>
  
  <div>
  <code>{{field.props['items'] | json}}</code>
  </div>

  `,
})
export class CustomSelect extends FieldType<FieldTypeConfig> implements OnInit {

  constructor(public formUtils: FormGeneratorService, private cd: ChangeDetectorRef) {
    super();
  }

  ngOnInit() {
    this.formUtils.initField(this.field, this.formControl);
  }
}
