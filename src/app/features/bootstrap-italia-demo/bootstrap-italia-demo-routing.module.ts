import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BootstrapItaliaDemoComponent } from './bootstrap-italia-demo.component';

const routes: Routes = [{ path: '', component: BootstrapItaliaDemoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BootstrapItaliaDemoRoutingModule { }
