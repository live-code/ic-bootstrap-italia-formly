import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../../shared/shared.module';

import { BootstrapItaliaDemoRoutingModule } from './bootstrap-italia-demo-routing.module';
import { BootstrapItaliaDemoComponent } from './bootstrap-italia-demo.component';
import { MyModalComponent } from './components/my-modal.component';


@NgModule({
  declarations: [
    BootstrapItaliaDemoComponent,
    MyModalComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgbModule,
    BootstrapItaliaDemoRoutingModule
  ]
})
export class BootstrapItaliaDemoModule { }
