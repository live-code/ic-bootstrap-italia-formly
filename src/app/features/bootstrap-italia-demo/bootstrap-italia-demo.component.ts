import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MyModalComponent } from './components/my-modal.component';

declare var bootstrap: any;


@Component({
  selector: 'app-bootstrap-italia-demo',
  template: `
    <button (click)="open()">Open</button>

    <app-icon size="xs" icon="it-check-circle"></app-icon>
    <app-icon size="xl" icon="it-check-circle"></app-icon>
    <app-icon size="xl" icon="it-pa"></app-icon>

    <div class="form-group">
      <label for="exampleInputText">Campo di tipo testuale</label>
      <input type="text" class="form-control" id="exampleInputText">
    </div>


    <div class="form-group">
      <label for="exampleInputText2">Campo di tipo testuale</label>
      <input type="text" class="form-control" id="exampleInputText2">
    </div>


    <ngb-datepicker #d></ngb-datepicker>

    <ngb-accordion [closeOthers]="true" activeIds="static-1">
      <ngb-panel id="static-1" title="Simple">
        <ng-template ngbPanelContent>
          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon
          officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
          moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim
          keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
          butcher
          vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't
          heard of them accusamus labore sustainable VHS.
        </ng-template>
      </ngb-panel>
      <ngb-panel id="static-2">
        <ng-template ngbPanelTitle>
          <span>&#9733; <b>Fancy</b> title &#9733;</span>
        </ng-template>
        <ng-template ngbPanelContent>
          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon
          officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
          moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim
          keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
          butcher
          vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't
          heard of them accusamus labore sustainable VHS.
        </ng-template>
      </ngb-panel>
      <ngb-panel id="static-3" title="Disabled" [disabled]="true">
        <ng-template ngbPanelContent>
          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon
          officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
          moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim
          keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
          butcher
          vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't
          heard of them accusamus labore sustainable VHS.
        </ng-template>
      </ngb-panel>
    </ngb-accordion>

    <!-- 2. datepicker in the popup -->
    <input type="text" ngbDatepicker #d="ngbDatepicker"/>
    <button type="button" class="btn btn-primary">Link</button>

    <div class="tooltip-demo">
      <p class="font-serif muted">
        Ecco un <a href="#">bianco scenario</a><br/>
        per tratteggiarvi l’accompagnamento<br/>
        degli oggetti di sfondo che pur vivono.<br/>
        Non ne sarò <a href="#" data-bs-toggle="tooltip" title="Secondo tooltip">l’artefice</a> impaziente.<br/>
        Berrò alle coppe della nostalgia,<br/>
        avrò preteso d’ozio nelle lacrime...<br/>
        perché non mi ribello alla natura:<br/>
        la mia lentezza li esaspera...<br/>
        La mia lentezza? No, la mia fiducia.<br/>
        Per adesso è deserto.<br/>
        <a href="#" data-bs-toggle="tooltip" title="Terzo tooltip">Il mondo può rifarsi senza me</a>,<br/>
        E intanto gli altri mi denigreranno
      </p>
      <small>
        <em>La città nuova, Alda Merini</em>
      </small>
    </div>
  `,
  styles: [
  ]
})
export class BootstrapItaliaDemoComponent {
  title = 'ic-bootstrap-italia';

  constructor(private modalService: NgbModal) {}

  open() {
    const modalRef = this.modalService.open(MyModalComponent);
    modalRef.componentInstance.name = 'World';
  }


  ngAfterViewInit() {
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
      return new bootstrap.Tooltip(tooltipTriggerEl)
    })
  }
}
