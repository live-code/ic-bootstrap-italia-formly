import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';

import { FormlyDemo2RoutingModule } from './formly-demo2-routing.module';
import { FormlyDemo2Component } from './formly-demo2.component';


@NgModule({
  declarations: [
    FormlyDemo2Component
  ],
  imports: [
    CommonModule,
    FormlyDemo2RoutingModule,
    ReactiveFormsModule,
    FormlyModule.forChild()
  ]
})
export class FormlyDemo2Module { }
