import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { Subject } from 'rxjs';
import { UsersService } from '../../core/users.service';
import {  MyService } from './config';

@Component({
  selector: 'app-formly-demo2',
  template: `
    <form [formGroup]="form" (ngSubmit)="submit()">
      <formly-form [model]="model" [fields]="fields" [options]="options" [form]="form"></formly-form>
      <button type="submit" class="btn btn-primary submit-button">Submit</button>
    </form>

    <pre>{{form.value | json}}</pre>
  `,
  styles: [
  ],
  providers: [
    MyService
  ]
})
export class FormlyDemo2Component  {
  private destroy$: Subject<any> = new Subject<any>();
  form = new FormGroup({});
  options: FormlyFormOptions = {};
  model: any;
  fields!: FormlyFieldConfig[];

  constructor(
    private http: HttpClient,
    private myService: MyService,
    private usersService: UsersService
  ) {
    http.get<any>('http://localhost:3000/user')
      .subscribe(res => {
        this.model = res;
        this.init()
      });
  }

  init() {
    // POSSIBILE EVOLUZIONE 1
    // const forms = ['anagrafica1', 'firstName', row: []'...']

    // NOTA: questi potrebbero diventare delle primitive 'firstName'
    // che sono usate dai preset ('anagrafica', ...)
    this.myService.setRootField({
      "key": "firstName",
      "type": "input",
      "props": {
        "label": "First Name",
        "onChange": {
          "debounce": 1000,
          "distinct": true,
          "service": 'usersService',
          "fn":'getUsers',
        },
      },
    })


    this.myService.setRootField(
      {
        "key": "lastName",
        "type": "input",
        "props": {
          "label": "Last Name",
        },
      },
    )
    /*

        this.myService.setGroupField('anagrafica', this.getAnagrafica())

     */
    this.fields = this.myService.formConfig;
  }

  submit() {
    if (this.form.valid) {
      alert(JSON.stringify(this.model));
    }
  }


  getAnagrafica() {
    return [
        this.myService.getField(
          {
            "key": "lastName",
            "type": "input",
            "props": {
              "label": "Last Name 2",
            },
          },
          {
            debounce: 1000,
            distinct: true,
            service: [this.usersService, 'getUsers']
          },
          (res, field) => {
            console.log('GET RESULT in PARENT', res)
            if (res === 'abc') {
              field.parent?.form?.get('firstName')?.disable()
            }
          },
        )
      ]
  }
}
