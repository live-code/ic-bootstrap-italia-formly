import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormlyDemo2Component } from './formly-demo2.component';

const routes: Routes = [{ path: '', component: FormlyDemo2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormlyDemo2RoutingModule { }
