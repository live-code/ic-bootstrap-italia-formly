import { Injectable, Injector } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged, EMPTY, iif, mergeMap, of } from 'rxjs';
import { UsersService } from '../../../core/users.service';

@Injectable({ providedIn: 'root'})
export class FormUtilsService {
  constructor(private injector: Injector) {
  }

  changeHandler(props: any, input: any) {
    if (props['onChange']) {
      const serviceKeyFromJSON = props['onChange']['service'];
      const serviceClassToUse: any = this.injector.get(SERVICES[serviceKeyFromJSON]);
      const fnFromJSON: any = props['onChange']['fn'];

      return input.valueChanges
        .pipe(
          debounceTime(props['debounce'] || 0),
          distinctUntilChanged(),
          /*mergeMap(() => iif(
              () => props['distinct'],
            distinctUntilChanged(),

          )),*/
          mergeMap(text => serviceClassToUse[fnFromJSON].apply(serviceClassToUse, [text]))
        )

    }
  }
}


const SERVICES: { [key: string]: any } = {
  ['usersService']: UsersService,
}
