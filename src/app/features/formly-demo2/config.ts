import { inject, Injectable } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { debounceTime, distinctUntilChanged, EMPTY, mergeMap, of } from 'rxjs';
import { UsersService } from '../../core/users.service';

interface RxJSConfig  {
  debounce: number ,
  distinct: boolean,
  service: [cls: any, fnName: string]
}

@Injectable({ providedIn: 'root'})
export class MyService {
  formConfig: FormlyFieldConfig[] = [];

  setRootField(
    fieldConfig: FormlyFieldConfig,
    rxjsConfig?: RxJSConfig | null,
    changeFn?: (text: string, field: FormlyFieldConfig) => void,
  ): FormlyFieldConfig {

    const field = this.getField(fieldConfig, rxjsConfig, changeFn)
    this.formConfig.push(field)
    return field;
  }

  setGroupField(key: string, fields: FormlyFieldConfig[]) {
    this.formConfig.push( {
      key,
      fieldGroup: fields
    })
  }

  getField(
    fieldConfig: FormlyFieldConfig,
    rxjsConfig?: RxJSConfig | null,
    changeFn?: (text: string, field: FormlyFieldConfig) => void,
  ): FormlyFieldConfig {
    const newFormlyFieldConfig = {
      ...fieldConfig,
      hooks: {
        onChanges: (field: FormlyFieldConfig) => {
          field.formControl?.valueChanges
            .pipe(
              /*debounceTime(rxjsConfig?.debounce || 0),
              // TODO: dovrebbe essere opzionale
              distinctUntilChanged(),
              mergeMap(value => {
                // FIX: si potrebbe usare un iif
                if (rxjsConfig) {
                  const serviceClass = rxjsConfig?.service[0];
                  const fn: any = rxjsConfig?.service[1];
                  return serviceClass[fn].apply(serviceClass, [value]);
                } else {
                  return EMPTY;
                }

              })*/
            )
            .subscribe((text: any) => {
              if (changeFn)
                changeFn(text, field);
            })
        }
      }
    };

    return newFormlyFieldConfig;
  }

}
