import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Component({
  selector: 'app-formly-demo1',
  template: `
    <form [formGroup]="form" (ngSubmit)="onSubmit(model)">
      <formly-form
        [form]="form" [fields]="fields" [model]="model"></formly-form>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    
    <pre>{{form.value | json}}</pre>
  `,
  styles: [
  ]
})
export class FormlyDemo1Component {
  form = new FormGroup({});
  model = { email: 'email@gmail.com' };

  // NEXT STEP
  // 1. Handle events
  // 2. Load FROM JSON
  fields: FormlyFieldConfig[] = [
    {
      key: 'email',
      type: 'input',
      props: {
        label: 'Email address',
        placeholder: 'Enter email',
        required: true,
        // onChange: // invoke function
      }
    },

    {
      key: 'address',
      fieldGroup: [
        {
          key: 'town',
          type: 'input',
          props: {
            required: true,
            type: 'text',
            label: 'Town',
          },
        },
      ],
    }

  ];

  change() {

  }

  onSubmit(model: any) {
    console.log(model);
  }
}
