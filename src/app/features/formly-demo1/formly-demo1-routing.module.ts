import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormlyDemo1Component } from './formly-demo1.component';

const routes: Routes = [{ path: '', component: FormlyDemo1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormlyDemo1RoutingModule { }
