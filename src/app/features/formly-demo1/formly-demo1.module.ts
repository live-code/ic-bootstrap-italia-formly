import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormlyModule } from '@ngx-formly/core';

import { FormlyDemo1RoutingModule } from './formly-demo1-routing.module';
import { FormlyDemo1Component } from './formly-demo1.component';


@NgModule({
  declarations: [
    FormlyDemo1Component
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormlyDemo1RoutingModule,
    FormlyModule.forChild()
  ]
})
export class FormlyDemo1Module { }
